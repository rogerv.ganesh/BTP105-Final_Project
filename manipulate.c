#include "manipulate.h"

void manipulating() {
	// version 1
	printf("*** Start of Concatenating Strings Demo ***\n");
	char string1[80] = "";
	char string2[80] = "";
	while (TRUE) {
		printf("Type the 1st string (q - to quit): ");
		gets(string1);
		if (!strcmp(string1, "q")) break;
		printf("Type the second string: ");
		gets(string2);
		strcat(string1, string2);
		printf("Concatenated string is \'%s\'\n", string1);
	}
	printf("*** End of Concatenating Strings Demo ***\n\n");

	// version 2
	printf("*** Start of Comparing Strings Demo ***\n");
	char compare1[80] = "";
	char compare2[80] = "";
	int result;
	while (TRUE) {
		printf("Type the 1st string to compare (q - to quit): ");
		gets(compare1);
		if (!strcmp(compare1, "q")) break;
		printf("Type the 2nd string to compare: ");
		gets(compare2);
		result = strcmp(compare1, compare2);
		if (result < 0) printf("1st string is less than 2nd\n");
		else if (result == 0) printf("1st string is equal to 2nd\n");
		else printf("1st string is greater than 2nd\n");
	}
	printf("*** End of Comparing Strings Demo ***\n\n");

	// version 3
	printf("*** Start of Searching Strings Demo ***\n");
	char big_string[80] = "";
	char sub_string[80] = "";
	char* address = NULL;
	while (TRUE) {
		printf("Type the big string (q - to quit): ");
		gets(big_string);
		if (!strcmp(big_string, "q")) break;
		printf("Type the substring: ");
		gets(sub_string);
		address = strstr(big_string, sub_string);
		if (address != NULL)
			printf("Found at %ld position\n", (long)address - (long)big_string);
		else printf("Not found\n");
	}
	printf("*** End of Searching Strings Demo ***\n\n");
}
