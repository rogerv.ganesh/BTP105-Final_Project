#include "converting.h"
#include "tokenizing.h"
#include "manipulate.h"
#include "fundamentals.h"

#include <stdio.h>
#include <string.h>

int main() {

  fundamentals();
  manipulating();
  converting();
  tokenizing();

  return 0;

}
